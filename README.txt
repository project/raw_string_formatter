This module adds a raw string formatter for field type "string_long".
You can select this formatter while configuring the display modes of an entity.